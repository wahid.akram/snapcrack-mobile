(ns snapcrack.localstorage
  (:require [clojure.spec.alpha :as s]
            [snapcrack.utils :as utils]
            [snapcrack.common.ui :refer [async-storage]]))

(defn write
  [key value & [success-fn error-fn]]
  (let [val (->> value
                 (clj->js)
                 (.stringify js/JSON))]
    (println "Writing localstorage: " key "=" val "...")
    (-> async-storage
        (.setItem key val)
        (.then (or success-fn identity)
               (or error-fn identity)))))

(defn -create-fetched-val-logger
  [key]
  (fn [val]
    (println (str "Read localstorage: " key "=" val))
    val))

(defn read
  [key & [success-fn error-fn]]
  (-> async-storage
      (.getItem key)
      (.then (or (comp success-fn
                       (-create-fetched-val-logger key)
                       utils/parse-json)
                 identity)
             (or error-fn identity))))

(defn delete
  [key & [success-fn error-fn]]
  (-> async-storage
      (.removeItem key)
      (.then (or (success-fn identity))
             (or (error-fn identity)))))
