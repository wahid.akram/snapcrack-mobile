(ns snapcrack.distance
  (:require [clojure.string :as str]
            [goog.string :as gstr]
            [applied-science.js-interop :as j]
            [re-frame.core :refer [dispatch]]
            [snapcrack.config :refer [google-maps-apikey]]
            [snapcrack.common.ui :refer [expo]]
            [snapcrack.utils :as utils]))

(def location (j/get expo :Location))

(def google-distance-matrix-api-url-format
  "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=%s&destinations=%s&key=%s")

(defn format-coords
  [marker]
  (let [{lat :latitude
         long :longitude} marker]
    (str lat "," long)))

(defn -select-nearest-marker
  [user-loc markers]
  (let [origin       (-> user-loc
                         (utils/to-clj)
                         (:coords)
                         (format-coords))
        destinations (map format-coords markers)
        url          (gstr/format google-distance-matrix-api-url-format
                                  origin
                                  (str/join "|" destinations)
                                  google-maps-apikey)]
    (-> url
        (js/fetch #js {:method  "GET"
                       :headers {:Accept       "application.json"
                                 :Content-Type "application/json"}})
        (j/call :then
                (fn [result]
                  (j/call result :json))
                (fn [error]
                  (println "[ERROR] Failed to calculate distances:" error)))
        (j/call :then
                (fn [json-result]
                  (let [r (utils/to-clj json-result)]
                    (if-let [error (:error_message r)]
                      (dispatch [:show-alert error :error])
                      (let [distances          (as-> r $
                                                 (get-in $ [:rows 0 :elements])
                                                 (map #(get-in % [:distance :value]) $))
                            [nearest-distance-index
                             nearest-distance] (apply min-key
                                                      #(second %)
                                                      (map-indexed vector distances))
                            nearest-marker     (nth markers nearest-distance-index)]
                        (dispatch [:select-marker nearest-distance-index])))))))))

(defn select-nearest-marker
  [markers]
  (-> location
      (j/call :requestPermissionsAsync)
      (j/call :then
              (fn [_]
                (-> location
                    (j/call :getCurrentPositionAsync
                            #js {:accuracy (j/get-in location [:Accuracy :BestForNavigation])})
                    (j/call :then
                            (fn [loc]
                              (-select-nearest-marker loc markers))
                            (fn [error]
                              (println "[ERROR] Failed to get current location:" error)))))
              (fn [error]
                (println "[ERROR] Location permission was not granted.")))))
