(ns snapcrack.screens.core
  (:require [snapcrack.screens.map-screen :as map-screen]
            [snapcrack.screens.auth_screen :as auth-screen]
            [snapcrack.screens.settings-screen :as settings-screen]
            [snapcrack.screens.booking-list :as booking-list]
            [snapcrack.screens.about-screen :as about-screen]
            [reagent.core :as r]
            [applied-science.js-interop :as j]
            [snapcrack.common.image :refer [logo]]
            [snapcrack.common.navigation :as nav]
            [snapcrack.screens.alertbox]
            [snapcrack.common.ui :refer [ReactNavigation image ic button touchable-highlight view keyboard KeyboardAvoidingView]]))

(def stack-navigator-raw
  (j/call ReactNavigation
          :createStackNavigator
          #js {:Home     #js {:screen map-screen/map-screen}
               :Auth     #js {:screen auth-screen/auth-screen}
               :SignIn   #js {:screen auth-screen/sign-in-screen}
               :SignUp   #js {:screen auth-screen/sign-up-screen}
               :Settings #js {:screen settings-screen/settings-screen}
               :Booking  #js {:screen booking-list/booking-list}
               :About  #js {:screen about-screen/about-screen}}
          #js {:initialRouteName "Home"}))

(def app-container-raw (.createAppContainer ReactNavigation stack-navigator-raw))

(defn app
  []
  [(r/adapt-react-class app-container-raw)
   {:ref #(reset! nav/navigator %)}])
