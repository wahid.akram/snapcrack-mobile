(ns snapcrack.subs
  (:require [re-frame.core :refer [reg-sub]]))

(reg-sub
 :get-greeting
 (fn [db _]
   (:greeting db)))


(reg-sub
  :show-settings-sub
  (fn [db _]
    (:show-settings? db)))

(reg-sub
  :get-user-info
  (fn [db _]
    (:user db)))

(reg-sub
  :markers
  (fn [db _]
    (:markers db)))

(reg-sub
 :selected-marker-data
 (fn [db _]
   (let [idx (:selected-marker-index db)]
     (get-in db [:markers idx]))))

(reg-sub
 :active-overlay-screen
 (fn [db _]
   (:active-overlay-screen db)))

(reg-sub
 :alert-message
 (fn [db _]
   (:alert-message db)))


(reg-sub
 :last-appointment
 (fn [db _]
   (let [last-ap (last (get-in db [:user :appointments]))]
     last-ap)))

(reg-sub
 :appointments
 (fn [db _]
   (get-in db [:user :appointments])))

(reg-sub
 :logged-in?
 (fn [db _]
   (boolean (get-in db [:user :userid]))))

;; TODO: Fixme.
#_(reg-sub
 :maker-has-appointment-today?
 (fn [db [_ marker-idx]]
   (let [marker  (get-in db [:markers marker-idx])
         marker-name (:name marker)
         marker-date (js/Date.)
         appointments (get-in db [:user :appointments])]
     ;; TODO: Fixme.
     (some #(if (= marker-name (:name %)))
           (get-in db [:user :appointments])))))
