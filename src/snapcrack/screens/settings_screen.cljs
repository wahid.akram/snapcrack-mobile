(ns snapcrack.screens.settings-screen
  (:require [snapcrack.common.ui :refer [view text image img-bg scroll-view
                                         list-view data-source ic touchable-highlight
                                         alert flat-list QRCode Modal]]
            [re-frame.core :refer [reg-event-db subscribe dispatch dispatch-sync]]
            [reagent.core :as r]
            [snapcrack.common.image :refer [profile]]
            [snapcrack.screens.qrcode :refer [qrcode-only]]
            [snapcrack.style :as st]))

(declare style)

(defn settings-screen-view
  []
  (fn []
    (let [user       @(subscribe [:get-user-info])
          logged-in? @(subscribe [:logged-in?])]
      [scroll-view {:style (:container style)}
       [view {:style (:body style)}
        [view {:margin 20}

         [view {:style {:flex-direction "column" :align-items "center"}}
          [image {:source     (if (nil? (:photo-url user))
                                profile
                                {:uri   (:photo-url user)
                                 :cache "force-cache"
                                 })
                  :resizeMode "cover"
                  :style      {:width           70
                               :height          70
                               :borderRadius    35
                               :backgroundColor "transparent"
                               :marginBottom    10}}]
          (if logged-in?
            [text {:style {:font-size   20
                           :paddingTop  10
                           :font-weight "bold"
                           :paddingLeft 20
                           :color       "gray"}}
             (if (nil? (:name user))
               (:email user)
               (:name user))]
            [view {:style {}}
             [touchable-highlight {:on-press      #(dispatch [:navigate "Auth"])
                                   :underlayColor "transparent"
                                   :style         {:backgroundColor "#F4E649"
                                                   :shadowColor     "rgba(0, 0, 0, 0.8)"
                                                   :shadowOpacity   0.9
                                                   :shadowOffset    {:height 1}
                                                   :padding         10
                                                   :borderRadius    10}}
              [text {:style {:font-size 18}} "Sign In"]]])]]]

       (if logged-in?
         [touchable-highlight {:underlayColor "lightgray"
                                        ;:onHideUnderlay (.-unhighlight separators)
                                        ;:onShowUnderlay (.-highlight separators)
                               :on-press      #(dispatch [:navigate "Booking"])}
          [view {:style {:flex-direction "row" :padding 15 :align-items "center" :backgroundColor "white"}}
           [ic {:name "md-calendar" :size 30 :color "black" :paddingLeft "5%"}]
           [text {:style {:paddingLeft "10%" :font-weight "bold"}} "Appointments"]]])

       [touchable-highlight {:underlayColor "lightgray"
                                        ;:onHideUnderlay (.-unhighlight separators)
                                        ;:onShowUnderlay (.-highlight separators)
                             :on-press      #(dispatch [:navigate "About"])}
        [view {:style {:flex-direction "row" :padding 15 :align-items "center" :backgroundColor "white"}}
         [ic {:name "ios-information-circle-outline" :size 30 :color "black" :paddingLeft "5%"}]
         [text {:style {:paddingLeft "10%" :font-weight "bold"}} "About"]]]

        (if logged-in?
          [touchable-highlight {:underlayColor "lightgray"
                                        ;:onHideUnderlay (.-unhighlight separators)
                                        ;:onShowUnderlay (.-highlight separators)
                                :on-press      #(dispatch [:show-qrcode])}
           [view {:style {:flex-direction  "row"
                          :padding         15
                          :align-items     "center"
                          :backgroundColor "white"}}
            [ic {:name  "md-barcode"
                 :color "black"
                 :size  30}]
                                        ;[view {:style {:overflow "hidden"}}
                                        ; [:> QRCode
                                        ;  {:bgColor "white"
                                        ;   :fgColor "black"
                                        ;   :size    30
                                        ;   :value   (:email user)}]]
            [text {:style {:paddingLeft "10%"
                           :font-weight "bold"}}
            "Check-in Code"]]])

       (if logged-in?
         [touchable-highlight {:underlayColor "lightgray"
                                        ;:onHideUnderlay (.-unhighlight separators)
                                        ;:onShowUnderlay (.-highlight separators)
                               :on-press      #(do (dispatch [:logout])
                                                   (dispatch [:show-alert "You have been logged out."]))}

          [view {:style {:flex-direction  "row"
                         :padding         15
                         :align-items     "center"
                         :backgroundColor "white"}}
           [ic {:name        "md-exit"
                :size        30
                :color       "black"
                :paddingLeft "5%"}]
           [text {:style {:paddingLeft "10%"
                          :font-weight "bold"
                          :color       "black"}}
            "Logout"]]])

       [qrcode-only]])))

(def style {:container {:flex            1
                        :backgroundColor "white"}
            :body      {:flex-direction  "column"
                        :height          150
                        :width           "100%"
                        :backgroundColor "#F4E649"}})
(def settings-screen (doto (r/reactify-component (settings-screen-view))
                       (aset "navigationOptions" #js {:title           "Profile"
                                                      :headerStyle     #js {:backgroundColor "#F4E649"}
                                                      :headerTintColor "black"})))
