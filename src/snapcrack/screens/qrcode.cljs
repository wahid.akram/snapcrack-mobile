(ns snapcrack.screens.qrcode
  (:require [re-frame.core :refer [subscribe reg-event-db dispatch]]
            [snapcrack.common.ui :refer [view text image img-bg scroll-view
                                         list-view data-source ic touchable-highlight
                                         alert flat-list QRCode Modal]]
            [clojure.string :as str]
            [snapcrack.utils :as utils]))

(reg-event-db
  :show-qrcode
  (fn [db [_]]
    (assoc db :active-overlay-screen :qrcode)))

(reg-event-db
 :show-qrcode-and-details
 (fn [db [_]]
   (assoc db :active-overlay-screen :qrcode-and-details)))

(reg-event-db
  :hide-qrcode
  (fn [db [_]]
    (assoc db :active-overlay-screen nil)))

(defn qrcode-with-optional-details
  [userinfo & [appointment-details marker-details]]
  #_(println userinfo)
  #_(println appointment-details)
  #_(println marker-details)
  [:> Modal {                                               ;:animationIn     "slideInUp"
             :onBackdropPress #(dispatch [:hide-qrcode])
             :isVisible       true}
   [view {:style {:align-items     "center"
                  :justify-content "center"
                  :backgroundColor "#282a36"}}
    [view {:style {:flex-direction  "row"
                   :height          40
                   :width           "100%"
                   :align-items     "center"
                   :justify-content "center"
                   :backgroundColor "#F4E649"}}
     [view {:style {:width "100%"}}
      [text {:style {:font-weight "bold"
                     :width       "100%"
                     :text-align  "center"}}
       "Check-in Code"]]]

    [view {:style {:padding 10}}
     (if appointment-details
       [view {:style {:flex-direction "column"
                      :align-items "flex-start"
                      :paddingBottom 5}}
        [view {:style {:flex-direction "row"
                       :align-items    "center"}}
         [text {:style {:padding     2
                        :color       "gray"
                        :font-weight "bold"}}
          "Location : "]
         [text {:style {:padding 2 :color "white"}}
          (:marker-name appointment-details)]]
        [view {:style {:flex-direction "row"
                       :align-items    "center"}}
         [text {:style {:padding     2
                        :color       "gray"
                        :font-weight "bold"}} "Date : "]
         [text {:style {:padding 2 :color "white"}}
          (utils/format-date (:date appointment-details))]]
        [view {:style {:flex-direction "row"
                       :align-items    "center"}}
         [text {:style {:padding     0
                        :color       "gray"
                        :font-weight "bold"}} "Status : "]
         [text {:style {:padding 2
                        :color   (case (:status appointment-details)
                                   "done"      "green"
                                   "cancelled" "red"
                                   "orange")}}
          (str/capitalize (:status appointment-details))]]])
     [view {:style {:overflow "hidden"}}
      [:> QRCode
       {:bgColor "white"
        :fgColor "black"
        :size    250
        :value   (:email userinfo)}]]]


    [touchable-highlight {:style         {:margin           10
                                          :borderRadius     30
                                          :background-color "#F4E649"
                                          :padding          10
                                          :align-items      "center"
                                          :justify-content  "center"}
                          :underlayColor "#F4E649"
                          :on-press      #(dispatch [:hide-qrcode])}
     [text {:style {:font-weight "bold"}}
      "Close"]]]])

(defn qrcode-only
  []
  (when (= @(subscribe [:active-overlay-screen]) :qrcode)
    (let [userinfo @(subscribe [:get-user-info])]
      [qrcode-with-optional-details userinfo])))

(defn qrcode-and-details
  []
  (when (= @(subscribe [:active-overlay-screen]) :qrcode-and-details)
    (let [userinfo            @(subscribe [:get-user-info])
          appointment-details @(subscribe [:last-appointment])
          marker-name         (:marker-name appointment-details)
          marker-details      (->> @(subscribe [:markers])
                                   (filter #(= marker-name (:name %)))
                                   (first))]
      [qrcode-with-optional-details
       userinfo
       appointment-details
       marker-details])))
