(ns snapcrack.screens.booking-list
  (:require [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [snapcrack.common.ui :refer [view flat-list touchable-highlight ic text activity-indicator]]
            [snapcrack.utils :as utils]
            [clojure.string :as str]))

(defn loading-screen
  [logged-in?]
  [view {:style {:flex            1
                 :align-items     "center"
                 :justify-content "center"}}

   [view
    [text {:style {}}
     "Your appointment will appear here."]]])

(defn item-flat-list [booking]
  (fn [booking]
    [view
     [flat-list
      {:data          (clj->js (reverse booking))
       :ItemSeparatorComponent
                      (fn [item-info-js]
                        (let [item-info (js->clj item-info-js :keywordize-keys true)
                              {:keys [highlighted]} item-info]
                          (r/as-element
                            [view {:style {:height          1
                                           :width           "100%"
                                           :backgroundColor "#CED0CE"}}])))
       :style         {:height "100%"}
       :key-extractor (fn [item index]
                        (str (:timestamp (js->clj item :keywordize-keys true))))
       :render-item   (fn [item-info-js]
                        (let [item-info (utils/to-clj item-info-js)
                              {:keys [item index separators]} item-info]
                          (r/as-element
                            [view [view {:style {:flex-direction  "row"
                                                 :padding         10
                                                 :backgroundColor "white"
                                                 ;:justify-content "space-between"
                                                 ;:shadow-offset   {:width 0 :height 2}
                                                 ;:shadow-opacity  0.8
                                                 }}
                                   [view {:style {:align-items "center" :justify-content "center"}}
                                    (if (= (:status item) "done")
                                      [ic {:name "md-checkmark-circle" :size 40 :color "green"}]
                                      (if (= (:status item) "cancelled")
                                        [ic {:name "md-close-circle"
                                             :size 40 :color "red"}]
                                        [ic {:name "md-information-circle-outline"
                                             :size 40 :color "orange"}]))]
                                   [view {:style {:flex-direction "column"
                                                  :paddingLeft    20}}
                                    [view {:style {:flex-direction "row"
                                                   :align-items    "center"}}
                                     [text {:style {:padding     2
                                                    :color       "gray"
                                                    :font-weight "bold"}}
                                      "Location : "]
                                     [text {:style {:padding 2}}
                                      (:marker-name item)]]
                                    [view {:style {:flex-direction "row"
                                                   :align-items    "center"}}
                                     [text {:style {:padding     2 :color "gray"
                                                    :font-weight "bold"}} "Date"]
                                     [text {:style {:padding 2}}
                                      (utils/format-date (:date item))]]
                                    [view {:style {:flex-direction "row"
                                                   :align-items    "center"}}
                                     [text {:style {:padding     0 :color "gray"
                                                    :font-weight "bold"}} "Status : "]
                                     [text {:style {:padding 2
                                                    :color   (if (= (:status item) "done")
                                                               "green"
                                                               (if (= (:status item) "cancelled")
                                                                 "red"
                                                                 "orange"))}}
                                      (str/capitalize (:status item))]]]]

                             (when (= (:status item) "pending")
                               [touchable-highlight {:underlayColor "transparent"
                                                     :on-press      #(dispatch [:cancel-appointment (:id item)])
                                                     :style         {:align-items "center" :padding 5}}
                                [text {:style {:color       "red"
                                               :font-weight "bold"}}
                                 "Cancel"]])]
                            )))}]]))
(defn booking-list-view
  []
  (fn []
    (let [appointments @(subscribe [:appointments])
          logged-in? @(subscribe [:logged-in?])]
      [view {:style {:flex 1}}
       (if (empty? appointments)
         [loading-screen logged-in?]
         [item-flat-list appointments])])))
(def booking-list (doto (r/reactify-component (booking-list-view))
                    (aset "navigationOptions" #js {:title           "Appointments"
                                                   :headerStyle     #js {:backgroundColor "#F4E649"}
                                                   :headerTintColor "black"})))
