(ns snapcrack.handlers
  (:require
    [re-frame.core :refer [reg-event-db ->interceptor reg-fx reg-event-fx dispatch]]
    [clojure.spec.alpha :as s]
    [snapcrack.firebase :as fb]
    [snapcrack.common.ui :refer [alert]]
    [snapcrack.db :as db :refer [app-db]]
    [snapcrack.localstorage :as ls]
    [snapcrack.screens.map-screen :as map-screen]
    [snapcrack.utils :as u]
    [snapcrack.distance :as distance]
    [snapcrack.config :as config]))

;; -- Interceptors ----------------------------------------------------------
;;
;; See https://github.com/Day8/re-frame/blob/develop/docs/Interceptors.md
;;
(defn check-and-throw
  "Throw an exception if db doesn't have a valid spec."
  [spec db]
  (when-not (s/valid? spec db)
    (let [explain-data (s/explain-data spec db)]
      (throw (ex-info (str "Spec check failed: " explain-data) explain-data)))))

(def validate-spec
  (if goog.DEBUG
    (->interceptor
      :id :validate-spec
      :after (fn [context]
               (let [db (-> context :effects :db)]
                 (check-and-throw ::db/app-db db)
                 context)))
    ->interceptor))

;; -- Handlers --------------------------------------------------------------

(reg-event-db
  :initialize-db
  [validate-spec]
  (fn [_ _]
    app-db))

(reg-event-db
  :set-greeting
  [validate-spec]
  (fn [db [_ value]]
    (assoc db :greeting value)))

(reg-event-db
  :show-settings-handlers
  ;[validate-spec]
  (fn [db [_ _]]
    (assoc db :show-settings? true)))

(reg-event-fx
  :add-firebase-user
  ;[validate-spec]
  (fn [{:as   cofx
        :keys [db]} [_ user]]
    {:db              db
     :call-async-func [fb/add-user-to-firebase (:email user) (:password user)]}))

(reg-event-fx
  :login-with-email
  ;[validate-spec]
  (fn [{:as   cofx
        :keys [db]} [_ user]]
    {:db              db
     :call-async-func [fb/login-with-email (:email user) (:password user)]}))

;; Updates reframe database and local-storage with auth-data from firebase.
(reg-event-fx
  :load-auth-from-firebase
  ;[validate-spec]
  (fn [{:as   cofx
        :keys [db]} [_ userinfo]]
    (let [userinfo (assoc userinfo :appointments [])
          new-db   (assoc db
                          :user userinfo
                          :selected-marker 0
                          :active-overlay-screen nil
                          :alert-message nil)]
      {:db         new-db
       :dispatch-n (if (:appointment-booking-initiated? db)
                     [[:write-db-to-localstorage]
                      [:navigate "Home"]
                      [:show-appointment-confirmation-dialogue]]
                     [[:write-db-to-localstorage]
                      [:navigate "Home"]])})))

;; Writes part of database to localstorage.
(reg-event-fx
 :write-db-to-localstorage
 (fn [{:as   cofx
       :keys [db]} [_]]
   (let [cdb (select-keys db config/localstorage-cacheable-db-keys)]
     {:db              db
      :call-async-func [ls/write "cache" cdb]})))

;; Loads `cached-db` into app db. Not to be used directly.
(reg-event-db
 :load-db-from-localstorage
 (fn [{:as cofx
       :keys [db]} [_ cached-db]]
   (merge db cached-db)))

;; Reads and loads cached app-db from localstorage.
(reg-event-fx
 :read-db-from-localstorage
 (fn [{:as   cofx
       :keys [db]} [_]]
   {:db              db
    :call-async-func [ls/read
                      "cache"
                      #(dispatch [:load-db-from-localstorage %])]}))

(reg-event-fx
  :login-with-facebook
  (fn [cofx [_]]
    (println "Logging in with facebook ...")
    {:db              (:db cofx)
     :call-async-func [fb/login-with-facebook]}))

(reg-event-fx
  :logout
  (fn [{:as   cofx
        :keys [db]} [_]]
    (println "Logging out ...")
    {:db              (assoc db :user {:appointments []})
     :call-async-func [fb/logout]}))

(reg-event-fx
  :load-markers-from-firebase
  (fn [{:as   cofx
        :keys [db]} [_ markers]]
    (let [new-db (assoc db
                        :markers markers
                        :selected-marker-index 0)]
      {:db new-db
       :dispatch-n [[:select-closest-marker]
                    [:write-db-to-localstorage]]})))

(reg-event-fx
 :load-appointments-from-firebase
 (fn [{:as   cofx
       :keys [db]} [_ appointments]]
   (let [new-db (assoc-in db [:user :appointments] appointments)]
     {:db new-db
      :dispatch [:write-db-to-localstorage]})))

(reg-event-fx
  :select-marker
  (fn [{:keys [db]
        :as   cofx} [_ idx]]
    (let [marker-data (get-in db [:markers idx])]
      {:db              (assoc db :selected-marker-index idx)
       :call-async-func [map-screen/show-selected-marker idx marker-data]})))

(reg-event-fx
 :select-closest-marker
 (fn [{:keys [db]
       :as   cofx} [_]]
   {:db db
    :call-async-func [distance/select-nearest-marker (:markers db)]}))

(reg-event-fx
  :confirm-appointment-booking
  (fn [{:as   cofx
        :keys [db]} [_]]
    (let [userid                  (get-in db [:user :userid])
          selected-marker-index   (:selected-marker-index db)
          selected-marker-data    (get-in db [:markers selected-marker-index])
          [appointment-id
           appointment-data]      (u/create-new-appointment userid selected-marker-data)
          last-appointment-status (some-> db
                                          (get-in [:user :appointments])
                                          (last)
                                          (:status))]
      (if (= last-appointment-status "pending")
        {:db       (assoc db :appointment-booking-initiated? false)
         :dispatch [:show-alert "Please cancel existing appointment to book a new one." :info]}
        {:db              (assoc db :appointment-booking-initiated? false)
         :call-async-func [fb/write-appointment-to-firebase
                           userid
                           appointment-id
                           appointment-data]}))))

(reg-event-fx
 :cancel-appointment
 (fn [{:as   cofx
       :keys [db]} [_ appointment-id]]
   (let [userid (get-in db [:user :userid])]
     {:db              db
      :call-async-func [fb/update-appointment-status-in-firebase
                        userid
                        appointment-id
                        "cancelled"]})))

(reg-event-fx
  :book-appointment
  (fn [{:as   cofx
        :keys [db]} [_]]
    (let [userid (get-in db [:user :userid])]
      (if-not userid
        {:db       (assoc db :appointment-booking-initiated? true)
         :dispatch [:navigate "Auth"]}
        {:db       db
         :dispatch [:show-appointment-confirmation-dialogue]}))))
