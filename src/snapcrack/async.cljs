(ns snapcrack.async
  (:require [reagent.core :as r]
            [re-frame.core :refer [reg-fx]]))

(reg-fx
 :call-async-func
 (fn [[func & args]]
   (println "Dispatching" (.-name func) "with" (count args) "args: " (vec args) "...")
   (apply func args)))
