(ns snapcrack.style)

(def map-marker-style
  {:height 80
   :width  80})

(def map-padding-size
  {:top    0
   :right  0
   :bottom 160
   :lef   0})

(def map-style
  {:height "100%"
   :width  "100%"})

(def map-marker-pin-color-default "red")
(def map-marker-pin-color-active  "yellow")

(def button-color-active "#333")
(def button-color-inactive "#ddd")

