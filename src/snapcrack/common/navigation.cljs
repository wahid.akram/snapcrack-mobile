(ns snapcrack.common.navigation
  (:require [reagent.core :as r]
            [goog.string :refer [format]]
            [applied-science.js-interop :as j]
            [re-frame.core :refer [reg-event-db reg-event-fx]]
            [snapcrack.common.ui :refer [ReactNavigation]]))

(def navigation-actions (j/get ReactNavigation :NavigationActions))
(def stack-actions (j/get ReactNavigation :StackActions))

(def navigator (r/atom nil))

(reg-event-fx
 :navigate
 (fn [cofx [_ route-name params]]
   (when @navigator
     (println "Navigating to: " route-name " ...")
     (.dispatch @navigator
                (.navigate navigation-actions
                           #js {:routeName route-name
                                :params     params})))
   (dissoc cofx :event)))

(reg-event-fx
 :navigate-back
 (fn [cofx [_]]
   (when @navigator
     (println "Navigating back ...")
     (.dispatch @navigator
                (.back navigation-actions)))
   (dissoc cofx :event)))

(reg-event-fx
 :pop-routes-from-navigation-stack
 (fn [cofx [_ n]]
   (when @navigator
     (println (format "Popping last %s navigation routes ..." n))
     (.dispatch @navigator
                (.pop stack-actions n)))
   (dissoc cofx :event)))
