(ns snapcrack.db
  (:require [clojure.spec.alpha :as s]
            [snapcrack.utils :as utils]))

;; spec of app-db
(s/def ::greeting string?)
(s/def ::app-db
  (s/keys :req-un [::greeting]))

(def valid-overlay-screens #{:settings
                             :alertbox
                             :appointment-details})

;; initial state of app-db
(def app-db {:greeting                       "Hello Clojurescript in Expo!"
             :user                           {:appointments []}
             :mapinfo                        {}
             :markers                        []
             :selected-marker                nil
             :active-overlay-screen          nil
             :alert-message                  nil
             :appointment-booking-initiated? false})
