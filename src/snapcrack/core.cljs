(ns snapcrack.core
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [applied-science.js-interop :as j]
            [snapcrack.handlers]
            [snapcrack.subs]
            [snapcrack.firebase :refer [setup-firebase]]
            [snapcrack.common.ui :refer [expo view text ReactNavigation expo Sentry KeyboardAvoidingView]]
            [snapcrack.screens.core :as sc]
            [snapcrack.async :as async]
            [snapcrack.config :as config]
            [snapcrack.distance]
            [cljs.pprint :as pp]))

(defn app-root []
  [sc/app])

(defn init []
  (.install
    (.config Sentry config/sentry-public-dns))
  (setup-firebase)
  (dispatch-sync [:initialize-db])
  (dispatch-sync [:read-db-from-localstorage])
  (j/call expo :registerRootComponent (r/reactify-component app-root)))
