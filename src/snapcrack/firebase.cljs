(ns snapcrack.firebase
  (:require [snapcrack.common.ui :refer [expo firebase firebase-auth firebase-fs]]
            [snapcrack.config :as config :refer [firebase-config]]
            [applied-science.js-interop :as j]
            [reagent.core :as r]
            [re-frame.core :refer [subscribe dispatch dispatch-sync reg-fx]]
            [snapcrack.utils :as utils]
            [clojure.string :as str]
            [goog.date :refer [Date DateTime]]))

(def firestore (r/atom nil))
(def firebase-unsub-funcs (r/atom {}))

(defn -promise-error-logger
  [error]
  (dispatch [:show-alert (str error) :error])
  (println (str "[PROMISE-ERROR] " error)))

(defn setup-appointment-change-hook
  [userid]
  (println "Setting up appointment change hook ...")
  (let [unsub-func (-> @firestore
                       (.collection "users")
                       (.doc userid)
                       (.collection "appointments")
                       (.onSnapshot
                         (fn [query-snapshot]
                           (let [appointments (->> query-snapshot
                                                   (.-docs)
                                                   (map #(-> %
                                                             (.data)
                                                             (utils/to-clj)
                                                             (update :timestamp utils/parse-datetime)
                                                             (update :date utils/parse-date)))
                                                   (vec))]
                             (dispatch [:load-appointments-from-firebase appointments])))))]
    (swap! firebase-unsub-funcs
           assoc
           :appointment-change
           unsub-func)))


(defn firebase-userobj->userinfo
  [userobj]
  {:email     (.-email userobj)
   :name      (.-displayName userobj)
   :photo-url (.-photoURL userobj)
   :userid    (.-uid userobj)})

(declare write-userinfo-to-firebase)
(defn setup-auth-state-change-hook
  "Setup hook to update reframe database on auth state change."
  []
  (-> firebase
      (.auth)
      (.onAuthStateChanged
        (fn [user]
          (if user
            (let [{:as   userinfo
                   :keys [userid]} (firebase-userobj->userinfo user)]
              (setup-appointment-change-hook userid)
              (write-userinfo-to-firebase userid userinfo)
              (dispatch [:load-auth-from-firebase userinfo]))
            (dispatch [:logout]))))))

(defn setup-marker-change-hook
  "Setup hook to synchronize markers from firebase."
  []
  (-> @firestore
      (.collection "markers")
      (.onSnapshot
        (fn [query-snapshot]
          (let [markers (->> query-snapshot
                             (.-docs)
                             (map #(-> % (.data) (utils/to-clj)))
                             (vec))]
            (dispatch [:load-markers-from-firebase markers]))))))

(defn setup-firebase
  []
  ;; Initialize application,
  (.initializeApp firebase firebase-config)

  ;; Create and attach the database object.
  (reset! firestore (.firestore firebase))

  ;; Change firebase authstate persistence to LOCAL.
  (-> firebase
      (.auth)
      (.setPersistence (j/get-in firebase [:auth :Auth :Persistence :LOCAL]))
      (.then identity
             -promise-error-logger))

  (setup-auth-state-change-hook)
  (setup-marker-change-hook))

(defn add-user-to-firebase
  [email password]
  (-> firebase
      ((.-auth firebase))
      (.createUserWithEmailAndPassword email password)
      (.catch
        -promise-error-logger
        ;(fn [error]
        ;  (let [errorCode (.-code error) errorMessage (.-message error)]
        ;    ;(println errorMessage)
        ;    ))
        )))

(defn login-with-email [email password]
  (->
    ((.-auth firebase))
    (.signInWithEmailAndPassword email password)
    (.catch
      -promise-error-logger
      ;(fn [error]
      ;  (let [errorCode (.-code error) errorMessage (.-message error)]
      ;    (println "testfb" (type errorCode))))
      )))

(defn write-to-firebase
  [path data & [success-fn failure-fn]]
  (loop [path path
         cursor @firestore]
    (let [[scheme val] (first path)]
      (case scheme
        :collection
        (recur (rest path) (.collection cursor val))
        :doc
        (recur (rest path) (.doc cursor val))
        nil
        (-> cursor
            (.set (clj->js data))
            (.then (or success-fn identity)
                   (or failure-fn identity)))
        :else
        (throw (js/Error. (str "Unknown firebase path scheme: " scheme)))))))

(defn delete-from-firebase
  [path & [success-fn failure-fn]]
  (loop [path path
         cursor @firestore]
    (let [[scheme val] (first path)]
      (case scheme
        :collection
        (recur (rest path) (.collection cursor val))
        :doc
        (recur (rest path) (.doc cursor val))
        nil
        (-> cursor
            (.delete)
            (.then (or success-fn identity)
                   (or failure-fn identity)))
        :else
        (throw (js/Error. (str "Unknown firebase path scheme: " scheme)))))))

(defn update-in-firebase
  [path data & [success-fn failure-fn]]
  (loop [path path
         cursor @firestore]
    (let [[scheme val] (first path)]
      (case scheme
        :collection
        (recur (rest path) (.collection cursor val))

        :doc
        (recur (rest path) (.doc cursor val))

        nil
        (-> cursor
            (.update (clj->js data))
            (.then (or success-fn identity)
                   (or failure-fn identity)))

        :else
        (throw (js/Error. (str "Unknown firebase path scheme: " scheme)))))))

(defn write-appointment-to-firebase
  [userid appointment-id appointment-data]
  (write-to-firebase [[:collection "users"]
                      [:doc userid]
                      [:collection "appointments"]
                      [:doc appointment-id]]
                     appointment-data))

(defn update-appointment-status-in-firebase
  [userid appointment-id status]
  (update-in-firebase [[:collection "users"]
                       [:doc userid]
                       [:collection "appointments"]
                       [:doc appointment-id]]
                      {:status status}
                      (fn [_]
                        (dispatch [:show-alert "Appointment has been cancelled." :info]))
                      -promise-error-logger))

(defn write-userinfo-to-firebase
  [userid userinfo]
  (write-to-firebase [[:collection "users"]
                      [:doc userid]]
                     {:userinfo userinfo}))

(defn fire-get
  [userid]
  (-> @firestore
      (.collection "users")
      (.doc userid)
      (.get)
      (.then
        (fn [result]
          (.log js/console (.data result))))))

(defn -signin-and-retrieve-data
  [credential]
  (-> firebase
      (.auth)
      (.signInAndRetrieveDataWithCredential credential)
      (.then (fn [_] (println "User signed in."))
             -promise-error-logger)))

(defn login-with-facebook
  []
  (let [login-params {:permissions ["public_profile" "email"]}]
    (-> (.-Facebook expo)
        (.logInWithReadPermissionsAsync config/facebook-app-id
                                        (js->clj login-params))
        (.then (fn [result]
                 (let [result (utils/to-clj result)]
                   (if (= (:type result) "cancel")
                     (println "User cancelled facebook login.")
                     (let [token (:token result)
                           credential (-> firebase
                                          (.. -auth -FacebookAuthProvider)
                                          (.credential token))]
                       (-signin-and-retrieve-data credential)))))
               -promise-error-logger))))

(defn login-with-google
  []
  (let [login-params {:clientId (if (= (utils/get-os-platform) "ios")
                                  config/google-ios-client-id
                                  config/google-android-client-id)
                      :behavior "web"
                      :scopes   ["profile" "email"]}]
    (-> (.-Google expo)
        (.logInAsync (clj->js login-params))
        (.then (fn [result]
                 (when (= (.-type result) "success")
                   (let [id-token (.-idToken result)
                         access-token (.-accessToken result)
                         credential (-> (.. firebase -auth -GoogleAuthProvider)
                                        (.credential id-token access-token))]
                     (-signin-and-retrieve-data credential))))
               -promise-error-logger))))

(defn logout
  []
  (when-let [unsub-func (:appointment-change @firebase-unsub-funcs)]
    (unsub-func))
  (-> firebase
      (.auth)
      (.signOut)
      (.then identity
             -promise-error-logger))
  (dispatch [:navigate "Home"]))
