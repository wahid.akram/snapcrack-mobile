(ns snapcrack.common.ui
  (:require [reagent.core :as r]
            [applied-science.js-interop :as j]))

(def ReactNative (js/require "react-native"))
(def expo (js/require "expo"))
(def AtExpo (js/require "@expo/vector-icons"))
(def ionicons (j/get AtExpo :Ionicons))
(def ic (r/adapt-react-class ionicons))
(def Modal (j/get (js/require "react-native-modal") :default))
(def QRCode (js/require "react-native-qrcode"))

(def text (r/adapt-react-class (j/get ReactNative :Text)))
(def keyboard (r/adapt-react-class (j/get ReactNative :Keyboard)))
(def KeyboardAvoidingView (r/adapt-react-class (j/get ReactNative :KeyboardAvoidingView)))
(def activity-indicator (r/adapt-react-class (j/get ReactNative :ActivityIndicator)))
(def view (r/adapt-react-class (j/get ReactNative :View)))
(def image (r/adapt-react-class (j/get ReactNative :Image)))
(def StatusBar (r/adapt-react-class (j/get ReactNative :StatusBar)))
(def touchable-highlight (r/adapt-react-class (j/get ReactNative :TouchableHighlight)))
(def Alert (j/get ReactNative :Alert))
(def text-input (r/adapt-react-class (j/get ReactNative :TextInput)))
(def button (r/adapt-react-class (j/get ReactNative :Button)))
(def img-bg (r/adapt-react-class (j/get ReactNative :ImageBackground)))
(def scroll-view (r/adapt-react-class (j/get ReactNative :ScrollView)))
(def list-view-raw (j/get ReactNative :ListView))
(def list-view (r/adapt-react-class list-view-raw))
(def data-source (j/get list-view-raw :DataSource))
(def async-storage (j/get ReactNative :AsyncStorage))

(def flat-list (r/adapt-react-class (j/get ReactNative :FlatList)))
(defn alert [title]
  (j/call Alert :alert title))
;MAP
(def map-view (r/adapt-react-class (j/get expo :MapView)))
(def map-marker (r/adapt-react-class (j/get-in expo [:MapView :Marker])))

;;firebase
(def firebase (js/require "firebase/app"))
(def firebase-auth (js/require "firebase/auth"))
(def firebase-db (js/require "firebase/database"))
(def firebase-fs (js/require "firebase/firestore"))

;;navigation
(def ReactNavigation (js/require "react-navigation"))
(def Carousel (j/get (js/require "react-native-snap-carousel") :default))

(def Sentry (.-default (js/require "sentry-expo")))
