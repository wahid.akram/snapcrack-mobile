(ns snapcrack.utils
  (:require [applied-science.js-interop :as j]
            [goog.string :refer [format]]
            [cljs-time.core :as ct]
            [cljs-time.coerce :as ctc]
            [cljs-time.format :as ctf]
            [snapcrack.common.ui :refer [ReactNative]]))

(defn get-text [e]
  (-> e .-nativeEvent .-text))

(defn to-clj [data]
  (js->clj data :keywordize-keys true))

(defn parse-json [item]
  (some->> item
           (.parse js/JSON)
           (to-clj)))

(def datetime-formatter (:date-time-no-ms ctf/formatters))
(def date-formatter (:date ctf/formatters))

(defn datetime->date
  [datetime-obj]
  (ctc/to-date datetime-obj))

(defn format-datetime
  [datetime-obj]
  (ctf/unparse datetime-formatter datetime-obj))

(defn format-date
  [date-obj]
  (ctf/unparse date-formatter date-obj))

(defn get-now
  []
  (ct/now))

(defn parse-datetime
  [datetime-str]
  (ctf/parse datetime-formatter datetime-str))

(defn parse-date
  [date-str]
  (ctf/parse date-formatter date-str))

(defn date->appointment-id
  [date-obj]
  (format-datetime date-obj))

(defn create-new-appointment
  [userid marker-data & [time]]
  (let [time (or time (get-now))
        id   (date->appointment-id time)]
    [id
     {:id          id
      :date        (format-date time)
      :timestamp   (format-datetime time)
      :marker-name (:name marker-data)
      :status      "pending"}]))

(defn get-os-platform
  []
  (j/get-in ReactNative [:Platform :OS]))
