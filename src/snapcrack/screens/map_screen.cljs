(ns snapcrack.screens.map-screen
  (:require [snapcrack.common.ui :refer [ReactNative map-view map-marker view alert
                                         text image touchable-highlight ic Carousel
                                         flat-list Modal QRCode activity-indicator]]
            [snapcrack.common.image :refer [smarker logo blacklogo smarker]]
            [applied-science.js-interop :as j]
            [reagent.core :as r]
            [re-frame.core :refer [reg-event-db subscribe dispatch dispatch-sync]]
            [snapcrack.config :as config]
            [snapcrack.db :as db :refer [app-db]]
            [snapcrack.screens.alertbox :refer [alertbox]]
            [snapcrack.screens.qrcode :refer [qrcode-and-details]]
            [snapcrack.utils :as u]
            [snapcrack.style :as st]
            [snapcrack.firebase :as fb]))

(def style
  {:map-marker {}
   :container  {:align-items     "center"
                :justify-content "flex-end"
                :flex            1}
   :map        {:height "100%"
                :width  "100%"}})

(def dimensions (j/get ReactNative :Dimensions))
(def width
  (.-width (.get dimensions "window")))
(def height
  (.-height (.get dimensions "window")))

(defn loading-screen
  []
  [view {:style {:flex            1
                 :align-items     "center"
                 :justify-content "center"
                 :backgroundColor "#212121"}}
   [activity-indicator {:color "#F4E649", :size "large"}]
   [text {:style {:color "white"}}
    "Loading..."]])

(reg-event-db
  :show-appointment-confirmation-dialogue
  (fn [db [_]]
    (assoc db :active-overlay-screen :appointment-confirmation-dialogue)))

(reg-event-db
  :hide-appointment-confirmation-dialogue
  (fn [db [_]]
    (assoc db :active-overlay-screen nil)))

(defn appointment-confirmation-dialogue
  []
  (let [visible? (= @(subscribe [:active-overlay-screen])
                    :appointment-confirmation-dialogue)
        selected-marker-data @(subscribe [:selected-marker-data])
        user @(subscribe [:get-user-info])]
    [:> Modal {:animationIn     "slideInUp"
               :onBackdropPress #(dispatch [:hide-appointment-confirmation-dialogue])
               :isVisible       visible?}
     [view {:style {:align-items     "center"
                    :justify-content "center"
                    :backgroundColor "#282a36"}}
      [view {:style {:flex-direction  "row"
                     :height          40
                     :width           "100%"
                     :align-items     "center"
                     :justify-content "center"
                     :backgroundColor "#F4E649"}}
       [view {:style {:width "100%"}}
        [text {:style {:font-weight "bold"
                       :width       "100%"
                       :text-align  "center"}}
         "Confirm"]]
       [touchable-highlight {:underlayColor "transparent"
                             :style         {:position     "absolute"
                                             :width        "100%"
                                             :align-items  "flex-end"
                                             :paddingRight 10}
                             :on-press      #(dispatch [:hide-appointment-confirmation-dialogue])}
        [ic {:name "md-close" :size 30 :color "black"}]]]
      [text {:style {:padding 20 :color "white"}}
       "You are checked in at " (:address selected-marker-data)]
      [touchable-highlight {:style         {:margin           10
                                            :borderRadius     30
                                            :background-color "#F4E649"
                                            :padding          10
                                            :align-items      "center"
                                            :justify-content  "center"}
                            :underlayColor "#F4E649"
                            :on-press      #(do (dispatch [:confirm-appointment-booking])
                                                (dispatch [:hide-appointment-confirmation-dialogue]))}
       [text {}
        "Confirm check in"]]]]))

(defn -get-map-coord-from-marker
  [marker-data]
  (select-keys marker-data [:latitude :longitude]))

(defn -get-map-region-from-marker
  [marker-data]
  (->> marker-data
       (-get-map-coord-from-marker)
       (merge {:latitudeDelta  config/map-default-latitude-delta
               :longitudeDelta config/map-default-longitude-delta})))

(declare map-view-ref)
(declare carousel-view-ref)
(defn show-selected-marker
  [marker-idx marker-data]
  ;; Move map region.
  (when @map-view-ref
    (.animateToRegion @map-view-ref
                      (clj->js (-get-map-region-from-marker marker-data))
                      config/map-region-transition-duration))

  ;; Sync carousel slide.
  (when @carousel-view-ref
    (.snapToItem @carousel-view-ref marker-idx)))

(def carousel-view-ref (r/atom nil))
(defn carousel-view [nav]
  (fn []
    (let [user @(subscribe [:get-user-info])
          markers @(subscribe [:markers])
          last-appointment @(subscribe [:last-appointment])
          booking-possible? (not= "pending" (:status last-appointment))
          booking-pending? (= "pending" (:status last-appointment))
          last-appointment-marker-name (:marker-name last-appointment)
          selected-marker-data @(subscribe [:selected-marker-data])]
      [:> Carousel
       {:data            (clj->js markers)
        :ref             #(reset! carousel-view-ref %)
        :on-snap-to-item #(dispatch [:select-marker %])
        :render-item     (fn [data]
                           (let [{marker :item} (u/to-clj data)
                                 appointment-booked? (= (:name marker)
                                                        last-appointment-marker-name)]
                             (r/as-element
                               [view {:style {:width           "100%"
                                              :backgroundColor "white"
                                              :padding         10
                                              :borderRadius    10}}
                                (if (and appointment-booked? booking-pending?)
                                  [view {:flex-direction  "row"
                                         :align-items     "center"
                                         :justify-content "center"}
                                   [ic {:name  "md-checkmark"
                                        :color "green"
                                        :size  20}]
                                   [text {:style {:color "gray"}}
                                    " Checked In"]])
                                [text {:style {:font-weight "bold" :color "gray"}} "Address"]
                                [text {:style {:color "black"}}
                                 (:address marker)]

                                [view {:style {:paddingTop 5}}
                                 (cond
                                   (and appointment-booked? booking-pending?)
                                   [touchable-highlight
                                    {:style         {:borderRadius     30
                                                     :background-color "#ffea00"
                                                     :height           30
                                                     :align-items      "center"
                                                     :justify-content  "center"
                                                     :margin           10}
                                     :underlayColor "yellow"
                                     :on-press      #(dispatch [:show-qrcode-and-details])}
                                    [text {} "View Appointment Details"]]


                                   booking-possible?
                                   [touchable-highlight
                                    {:style         {:borderRadius     30
                                                     :background-color "#ffea00"
                                                     :height           30
                                                     :align-items      "center"
                                                     :justify-content  "center"
                                                     :margin           10}
                                     :underlayColor "yellow"
                                     :on-press      #(dispatch [:book-appointment])}
                                    [text {} "Check In"]]

                                   :else
                                   [touchable-highlight
                                    {:underlayColor "lightgrey"
                                     :style         {:borderRadius     30
                                                     :background-color "lightgrey"
                                                     :height           30
                                                     :align-items      "center"
                                                     :justify-content  "center"
                                                     :margin           10}
                                     :on-press      #(dispatch [:show-alert "Please cancel existing appointment to book a new one." :info])}
                                    [text {}
                                     "Check In"]])]])))

        :slider-width    width
        :item-width      (- width 70)}])))

(def map-view-ref (r/atom nil))
(defn map-screen-view []
  (fn [{:as   props
        :keys [navigation]}]
    (let [markers @(subscribe [:markers])
          last-appointment @(subscribe [:last-appointment])
          selected-marker-data @(subscribe [:selected-marker-data])]
      (println "platform" (j/get-in ReactNative [:Platform :OS]))
      (if-not (seq markers)
        ;; Show loading screen, as markers are yet to load.
        [loading-screen]

        ;; Show map view.
        [view {:style (:container style)}
         [map-view
          {:provider              "google"
           :ref                   #(reset! map-view-ref %)
           :custom-map-style      (js/require "./assets/cm.json")
           :map-padding           st/map-padding-size
           :style                 st/map-style
           :showsUserLocation     true
           :showsMyLocationButton true
           :showsCompass          true
           :showsBuildings        false
           :initial-region        (clj->js (-get-map-region-from-marker selected-marker-data))}
          (doall (for [[idx marker-data] (map-indexed vector markers)]
                   (let [last-appointment @(subscribe [:last-appointment])
                         last-appointment-marker-name (:marker-name last-appointment)
                         booking-possible? (not= "pending" (:status last-appointment))
                         appointment-booked? (= (:name marker-data)
                                                last-appointment-marker-name)
                         active-marker? (and appointment-booked? (not booking-possible?))]

                     [map-marker
                      {:coordinate       (-get-map-coord-from-marker marker-data)
                       ;; NOTE: The key must be changed for pin-color change to work in android.
                       ;;       See: https://github.com/react-native-community/react-native-maps/issues/887
                       :key              (if active-marker?
                                           (str idx "-marker-active")
                                           (str idx "-marker-inactive"))
                       :pin-color        (if active-marker?
                                           st/map-marker-pin-color-active
                                           st/map-marker-pin-color-default)
                       :on-press         #(dispatch [:select-marker idx])
                       :on-callout-press #(dispatch [:book-appointment])
                       :style            st/map-marker-style
                       :title            "Snap Crack"

                       }
                      [view {:style {;:background-color "white"
                                     :width  80
                                     :height 80
                                     ;:borderRadius     25
                                     }}
                       [image {:source smarker
                               :resizeMode "cover"
                               :style {:height 80
                                       :width 80}}]
                       ;[text "Snap\nCrack"]
                       ]
                      ])))]

         [view
          {:style {:position        "absolute"
                   :justify-content "center"
                   :padding         20}}

          [carousel-view navigation]]
         [appointment-confirmation-dialogue]
         [alertbox]
         [qrcode-and-details]]))))



(defn right-comp [props]
  (fn [props]
    (let [navigation (aget props "navigation")
          logged-in? @(subscribe [:logged-in?])]
      [view {:style {:flex-direction "row"}}
       (if logged-in?
         [touchable-highlight {:style         {:paddingRight 25}
                               :underlayColor "transparent"
                               :on-press      #(dispatch [:navigate "Booking"])}
          [ic {:name  "md-calendar"
               :size  30
               :color "black"}]])

       [touchable-highlight {:style         {:paddingRight 10}
                             :underlayColor "transparent"
                             :on-press      #(dispatch [:navigate "Settings"])}
        [ic {:name "md-person" :size 30 :color "black"}]]])))



(defn snapcrack-logo []
  (let []
    (fn []
      [image {:source blacklogo
              :style  {:width 120 :height 40}}])))

(def map-screen (let [x (r/reactify-component (map-screen-view))]
                  (aset x "navigationOptions" (fn [navigation]
                                                #js {:headerTitle     (r/reactify-component snapcrack-logo)
                                                     :headerStyle     #js {:backgroundColor "#F4E649"}
                                                     :headerRight     (r/as-element [right-comp navigation])
                                                     :headerTintColor "black"}))
                  x))
