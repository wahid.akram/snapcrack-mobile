(ns snapcrack.common.image
  (:require [snapcrack.common.ui :refer [image]]))

(def logo (js/require "./assets/images/logo/image.png"))
(def blacklogo (js/require "./assets/images/blacklogo/image.png"))
(def auth-bg (js/require "./assets/images/auth/image.png"))
(def google-logo (js/require "./assets/images/google/image.png"))
(def smarker (js/require "./assets/images/smarker/image.png"))
(def profile (js/require "./assets/images/profile/image.png"))
(def about (js/require "./assets/images/about/image.png"))