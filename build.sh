#!/bin/sh

PLATFORM=$1

USAGE="./build.sh [ios|android]\n"

export NODE_OPTIONS="--max_old_space_size=9192"

if [ "$PLATFORM" = "android" ] ; then
    lein clean && lein prod-build && expo build:android
elif [ "$PLATFORM" = "ios" ] ; then
    lein clean && lein prod-build && expo build:ios
else
     echo $USAGE;
     exit 1
fi
