(ns snapcrack.screens.alertbox
  (:require [snapcrack.common.ui
             :refer [Modal
                     view
                     text
                     image
                     touchable-highlight]]
            [reagent.core :as r]
            [re-frame.core
             :refer [reg-event-db
                     subscribe
                     dispatch
                     dispatch-sync]]))

;; Pop-ups an alertbox.
(reg-event-db
 :show-alert
 (fn [db [_ msg & [level]]]
   (let [level (or level :info)]
     (assoc db
            :alert-message [msg level]
            :active-overlay-screen :alertbox))))

;; Hides the alertbox.
(reg-event-db
 :clear-alert
 (fn [db [_]]
   (assoc db
          :alert-message nil
          :active-overlay-screen nil)))

(declare style)
(defn alertbox []
  (let [visible?        (= @(subscribe [:active-overlay-screen]) :alertbox)
        [msg & [level]] @(subscribe [:alert-message])
        level           (or level :info)]
    [:> Modal {:animation-in      "slideInUp"
               :on-backdrop-press #(dispatch [:clear-alert])
               :is-visible        visible?}

     [view {:style {:align-items     "center"
                    :justify-content "center"
                    :backgroundColor "#282a36"}}

      [view {:style {:height          40
                     :width           "100%"
                     :align-items     "center"
                     :justify-content "center"
                     :backgroundColor "#F4E649"}}
       [text {:style (:title style)}
        (case level
          :info  "Info"
          :error "Error"
          "Info")]]

      [text
       {:style {:padding 20
                :color   "white"}}
       msg]
      [touchable-highlight {:underlayColor "yellow"
                            :style {:margin           10
                                    :border-radius    30
                                    :background-color "#F4E649"

                                    :width           100
                                    :height          30
                                    :align-items     "center"
                                    :justify-content "center"}
                            :on-press #(dispatch [:clear-alert])}
       [text {:style (:signin-text style)}
        "OK"]]]]))

(def style {:container {:align-items     "center"
                        :justify-content "flex-end"
                        :flex            1}
            :map       {:height "100%"
                        :width  "100%"}

            :title {:font-weight "bold"}
            }
  )
