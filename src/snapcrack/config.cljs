(ns snapcrack.config)


(def firebase-config
  #js
      {:apiKey            ""
       :authDomain        ""
       :databaseURL       ""
       :projectId         ""
       :storageBucket     ""
       :messagingSenderId ""})

(def facebook-app-id "")

(def google-maps-apikey "")
(def google-ios-client-id "")
(def google-android-client-id "")

(def map-default-latitude-delta 0.04)
(def map-default-longitude-delta 0.04)

(def map-region-transition-duration 1000)

(def localstorage-cacheable-db-keys [:user :markers :selected-marker-index])

(def sentry-public-dns "")
