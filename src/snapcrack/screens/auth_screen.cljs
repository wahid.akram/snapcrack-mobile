(ns snapcrack.screens.auth_screen
  (:require [reagent.core :as r]
            [snapcrack.common.ui :refer [view touchable-highlight text-input image text ic expo img-bg alert StatusBar KeyboardAvoidingView]]
            [snapcrack.common.image :refer [logo auth-bg google-logo]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [snapcrack.handlers]
            [snapcrack.firebase :as fb]
            [clojure.string :as string]
            [snapcrack.utils :as u]))


(declare style)

(defn auth-screen-view []
  (fn [{:as   props
        :keys [navigation]}]
    [StatusBar {:backgroundColor "blue" :barStyle "light-content"}]
    [img-bg {:source auth-bg :style (:container style)}
     [view {:flex        1
            :align-items "flex-start"
            :style       {:width "100%" :background-color "rgba(52, 52, 52, 0.8)"}}
      [view {:style {:flex-direction "row" :marginTop 40 :marginLeft 20 :align-items "flex-start"}}

       [touchable-highlight {:style         {:paddingRight 10}
                             :underlayColor "transparent"
                             :on-press      #(dispatch [:navigate-back])}
        [ic {:name "md-close" :size 30 :color "white"}]]]

      [view {:style (:body style)}
       [image {:source logo
               :style  (:logo style)}]

       [touchable-highlight {:style         (:signin style)
                             :underlayColor "#FFFF99"
                             :on-press      #(dispatch [:navigate "SignUp"])}
        [view {:style {:flex-direction "row" :align-items "center" :justify-content "center"}}
         [ic {:name "ios-mail" :size 30 :color "black" :style {:paddingRight 10}}]
         [text {:style (:signin-text style)} "Sign Up with Email"]]]

       [touchable-highlight {:style         (:facebook style)
                             :underlayColor "#4b5f88"
                             :on-press      #(dispatch [:login-with-facebook])}
        [view {:style {:flex-direction "row" :align-items "center" :justify-content "center"}}
         [ic {:name "logo-facebook" :size 30 :color "white" :style {:paddingRight 10}}]
         [text {:style (:facebook-text style)} "Continue with facebook"]]]
       [touchable-highlight {:style         (:google style)
                             :underlayColor "#ff7f7f"
                             :on-press      #(fb/login-with-google)}
        [view {:style {:flex-direction "row" :align-items "center" :justify-content "center"}}
         [image {:source google-logo
                 :style  {:height 30 :width 30}}]
         [text {:style (:google-text style)} "Continue with Google"]]]

       [view {:style {:flex-direction "row" :paddingTop 10}}
        [text {:style {:color     "white"
                       :font-size 15}} "Have an account? "]
        [text {:onPress            #(dispatch [:navigate "SignIn"])
               :textDecorationLine "underline"
               :style              {:color "white" :font-size 15 :font-weight "bold"}} "Log In"]]]]]))


(defn sign-up-screen-view []
  (let [state (r/atom {:email    ""
                       :password ""})
        on-input-change (fn [key value]
                          (swap! state assoc key (if (= key :email)
                                                   (string/lower-case value)
                                                   value)))
        on-press (fn [event]
                   (let [{:keys [email password]} @state
                         invalid-input? (or (empty? email)
                                            (empty? password))
                         user (when-not invalid-input?
                                {:email    (->> (:email @state)
                                                string/trim)
                                 :password (:password @state)})]
                     (if-not invalid-input?
                       (dispatch [event user])
                       (println "Please enter both email and password."))))]
    (fn [{:as   props
          :keys [navigation]}]
      [img-bg {:source auth-bg :style (:container style)}
       [view {:flex        1
              :align-items "flex-start"
              :style       {:width "100%" :background-color "rgba(52, 52, 52, 0.8)"}}
        [view {:style {:flex-direction "row" :marginTop 40 :marginLeft 20 :align-items "flex-start"}}

         [touchable-highlight {:style         {:paddingRight 10}
                               :underlayColor "transparent"
                               :on-press      #(dispatch [:navigate-back])}
          [ic {:name "md-close" :size 30 :color "white"}]]]

        [KeyboardAvoidingView {:style (:body style) :behavior "padding"}
         [view {:style (:body style)}
          ;[image {:source logo
          ;        :style  (:logo style)}]
          [text {:style {:color "white" :font-size 30 :marginBottom 30}} "Sign Up"]
          [text-input {:underlineColorAndroid "transparent"
                       :placeholder           "Email"
                       :placeholderTextColor  "gray"
                       :auto-capitalize       "none"
                       :on-change             #(on-input-change :email (u/get-text %))
                       :style                 (:input style)}]
          [text-input {:secureTextEntry       true
                       :underlineColorAndroid "transparent"
                       :placeholderTextColor  "gray"
                       :placeholder           "Password"
                       :auto-capitalize       "none"
                       :on-change             #(on-input-change :password (u/get-text %))
                       :style                 (:input style)}]
          [touchable-highlight {:style         (:signin style)
                                :underlayColor "#FFFF99"
                                :on-press      #(on-press :add-firebase-user)}
           [text {:style (:signin-text style)} "Sign Up with Email"]]]]]])))






(defn sign-in-screen-view []
  (let [state (r/atom {:email    ""
                       :password ""})
        on-input-change (fn [key value]
                          (swap! state assoc key (if (= key :email)
                                                   (string/lower-case value)
                                                   value)))
        on-press (fn [event]
                   (let [{:keys [email password]} @state
                         invalid-input? (or (empty? email)
                                            (empty? password))
                         user (when-not invalid-input?
                                {:email    (->> (:email @state)
                                                string/trim)
                                 :password (:password @state)})]
                     (if-not invalid-input?
                       (dispatch [event user])
                       (println "Please enter both email and password."))))]
    (fn [{:as   props
          :keys [navigation]}]
      [img-bg {:source auth-bg :style (:container style)}
       [view {:flex        1
              :align-items "flex-start"
              :style       {:width "100%" :background-color "rgba(52, 52, 52, 0.8)"}}
        [view {:style {:flex-direction "row" :marginTop 40 :marginLeft 20 :align-items "flex-start"}}

         [touchable-highlight {:style         {:paddingRight 10}
                               :underlayColor "transparent"
                               :on-press      #(dispatch [:navigate-back])}
          [ic {:name "md-close" :size 30 :color "white"}]]]

        [KeyboardAvoidingView {:style (:body style) :behavior "padding"}
         [view {:style (:body style)}
          ;[image {:source logo
          ;        :style  (:logo style)}]
          [text {:style {:color "white" :font-size 30 :marginBottom 30}} "Sign In"]
          [text-input {:underlineColorAndroid "transparent"
                       :placeholder           "Email"
                       :placeholderTextColor  "gray"
                       :auto-capitalize       "none"
                       :on-change             #(on-input-change :email (u/get-text %))
                       :style                 (:input style)}]
          [text-input {:secureTextEntry       true
                       :underlineColorAndroid "transparent"
                       :placeholderTextColor  "gray"
                       :placeholder           "Password"
                       :auto-capitalize       "none"
                       :on-change             #(on-input-change :password (u/get-text %))
                       :style                 (:input style)}]
          [touchable-highlight {:style         (:signin style)
                                :underlayColor "#FFFF99"
                                :on-press      #(on-press :login-with-email)}
           [text {:style (:signin-text style)} "Sign In with Email"]]]]]])))




(defn snapcrack-logo []
  (let []
    (fn []
      [image {:source logo
              :style  {:width 120 :height 40}}])))



(def auth-screen (doto (r/reactify-component (auth-screen-view))
                   (aset "navigationOptions" #js {:header nil})))
                                                  ;:headerTitle (r/reactify-component snapcrack-logo)
                                                  ;:headerStyle #js {:backgroundColor "black"}
                                                  ;:headerTintColor "white"


(def sign-up-screen (doto (r/reactify-component (sign-up-screen-view))
                      (aset "navigationOptions" #js {:header nil})))
                                                     ;:headerTitle (r/reactify-component snapcrack-logo)
                                                     ;:headerStyle #js {:backgroundColor "black"}
                                                     ;:headerTintColor "white"



(def sign-in-screen (doto (r/reactify-component (sign-in-screen-view))
                      (aset "navigationOptions" #js {:header nil})))
                                                     ;:headerTitle (r/reactify-component snapcrack-logo)
                                                     ;:headerStyle #js {:backgroundColor "black"}
                                                     ;:headerTintColor "white"


(def style {:container     {:align-items     "center"
                            :justify-content "center"
                            :flex            1
                            :backgroundColor "black"}
            :text-input    {:height      40
                            :width       100
                            :borderColor "white"
                            :borderWidth 1}
            :body          {:borderRadius    30
                            :flex            1

                            :align-items     "center"
                            :justify-content "center"
                            :width           "100%"}
            :logo          {:width  230
                            :height 150}


            :input         {:margin            5
                            :text-align        "center"
                            :height            45
                            :width             250
                            ;:backgroundColor "white"
                            :borderColor       "white"
                            :borderBottomWidth 1
                            :font-weight       "bold"
                            :color             "white"}

            :signin-text   {:text-align  "center"
                            :font-weight "bold"}

            :signin        {:margin           10
                            :borderRadius     30
                            :background-color "yellow"
                            :padding          10
                            :width            250
                            :height           50
                            :justify-content  "center"}
            :or            {:padding         10
                            :flex-direction  "row"
                            :justify-content "center"
                            :align-items     "center"}
            :or-text       {:color       "white"
                            :font-weight "bold"
                            :font-size   15}
            :or-border     {:backgroundColor "white"
                            :height          1
                            :width           50}
            :facebook-text {:text-align  "center"
                            :font-weight "bold"
                            :color       "white"}

            :facebook      {:margin           10
                            :borderRadius     30
                            :background-color "#3B5998"
                            :padding          10
                            :width            250
                            :height           50
                            :justify-content  "center"}

            :google-text   {:text-align  "center"
                            :font-weight "bold"
                            :paddingLeft 6}
            :google        {:margin           10
                            :borderRadius     30
                            :background-color "white"
                            :padding          10
                            :width            250
                            :height           50
                            :justify-content  "center"}})


